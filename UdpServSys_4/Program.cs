﻿using System;

namespace UdpServSys_4
{
    class Program
    {
        static void Main(string[] args)
        {
            try // лловим исключение
            {
                new LoggerClass().Logging(" Info: Начло работы UDP-сервера", null); // создаем лог при запуске приложения
                Gatherer gatherer = new Gatherer(new UdpClass(9000), new ImplementClass()); // создаем объект udp с портом 9000 и объектом реализации
                gatherer.ThreadUdpReceiver().Start(); // запускаем прием пакетов от удаленног клиента в отдельном потоке
            }
            catch(Exception e)
            {
                new LoggerClass().Logging(" FATAL: ошибка запуска службы (уничтожаю процесс): ", e); // если при инициализации поймали исключение логируем
                System.Diagnostics.Process.GetCurrentProcess().Kill(); // убиваем процесс
            }
   
        }
    }
}

﻿using System;
using System.Threading.Tasks;

namespace UdpServSys_4
{
    delegate void SendData(string s); // это делегат для предоставления методов из другого класса-объекта

    class ImplementClass
    {
        object lockerDb = new object(); // определим локкер - блокиратор. Т.е. пока он каким-то фоновым потоком занят, то другой фоновый поток не будет использовать методы этого класса. Ну, это сделано, что-то вроде для синхронизации потоков при доступе к единным данным (БД)

        public event SendData ETxData = null; // событие отправки

        IRequest request; 

        public ImplementClass()
        {
            request = new Request(); при создании экземпляра класса инициализируем объект requst - запрос и работе с БД
        }

        private void UpdateDevice(string s) // метод обновления данных удаленного клиента в БД
        {
            lock(lockerDb) // захватываем локер
            {
                string result = null; 
                string[] packege = s.Split('#'); // принятые данные для записи в БД делим по символу "#"
                var r = request.UpdateRequest(packege[0], packege[1], packege[2]); // вызываем метод обновления данных варгументы передаем новые значения
                result = packege[0] + "#" + packege[1] + "#" + r;// формируем строку отправки
                if (r != null & ETxData != null)
                {
                    ETxData?.BeginInvoke(result, null, null);//  Вызываем асинхронно обработчик события отправки с ответом
                }
            }
        }    

        private void UpdateStatus() // обновляем статус удаленного клиента в БД
        {
                request.UpdateStatusDev();
        }

        private void SelectMsgDevice()// это метод отправки сообщений удаленному клиенту. Допустим, какая-то команда на выполнение, отправленная с диспетчерского центра
        {
            var r = request.SelectMessageDev();
            if (r != null)
            {
                string[] packege = r.Split(';');
                string result = null;
                for (int i = 0; i < packege.Length - 1; i++)
                {
                    string[] dataPackege = packege[i].Split('-');
                    result = dataPackege[0] + "#" + dataPackege[1] + "#" + dataPackege[2] + ":" + "3" + ":" + "0" + ":" + dataPackege[3];
                    Console.WriteLine(result);
                    if (ETxData != null)
                    {
                        ETxData?.BeginInvoke(result, null, null);
                    }

                }
            }
        }

        public async void HandlerReceiverUdpAsync(string s)
        {
            await Task.Run(() => UpdateDevice(s));
        }

        public async void HandlerSendMessageAsync(object o)
        {
            await Task.Run(() => UpdateStatus());
            await Task.Run(() => SelectMsgDevice());
        }

    }
}

﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace UdpServSys_4
{
    delegate void Receiver(string s);

    abstract class AUdpClass // определим абстрактный класс, от него будем наследоваться и переопределять методы
    {
        public abstract void ReceiverData(); // метод приема данных
        public abstract void SenderDataAsync(string s); // метод, реализующий отправку данных
    }

    // создаем класс UDP, унаследовавшись от абстактного класса
    class UdpClass : AUdpClass
    {
        public event Receiver ERxData = null; // это событие, возникающее по приему данных

        private int PropertyLPort { set; get; } // геттер и сеттер для записи и считываний значений

        public UdpClass(int port) // при вызове конструктора будем передавать параметр - порт, который будем слушать
        {
            PropertyLPort = port;
        }

        public override void ReceiverData() // переопределяем метод приема данных
        {
            UdpClient udp = new UdpClient(PropertyLPort); //создадим UDP клиент с нужным нам портом
            IPEndPoint remoteAdr = null; // определим объект для удаленного клиента

            try // ловим ошибки
            {
                while (true) // проверять прием данных будем в бесконечном цикле
                {
                    byte[] data = udp.Receive(ref remoteAdr); // запускаем метод приема данных, пока нет данных находимся здесь
                    Task.Run(() => { // как только они пришли создаем таск и сразу же его запускаем в фоновом потоке, чтобы отпустить основной поток и не было зависаний программы

                        string strOut = remoteAdr.Address + "#" + remoteAdr.Port + "#" + Encoding.ASCII.GetString(data); // тут просто формируем строку ответа, преобразуя пришедшие данные к формату ASCII
                        Console.WriteLine(strOut); // выводим в консоль
                        if (ERxData != null) // если событие созданно, а мы его создали
                        {
                            ERxData?.BeginInvoke(strOut, null, null); // запускаем собитие асинхронно, передав туда пришедшие данные
                        }

                    });
                }
            }
            catch (Exception e)
            {
                new LoggerClass().Logging(" ERROR: в методе приема сообщений обнаружены ошибки: ", e); // если поймали ошибку, логгируем
            }
            finally
            {
                udp.Close(); // обязательно закрываем соединение
            }
        }

        public async override void SenderDataAsync(string s) // переопределяем функцию отправки данных, делаем ее асинхронной, передаем в параметр строку ответа
        {
            await Task.Run(() => { // запускаем асинхронно таск
                UdpClient client = new UdpClient(); // создаем объект клиента UDP

                try // ловим ошибку
                {
                    string[] parSendData = s.Split('#');  // разделяем строку на части по символам "#"
                    byte[] dataOut = Encoding.ASCII.GetBytes(parSendData[2]); // переводим в байты
                    client.Send(dataOut, dataOut.Length, parSendData[0], int.Parse(parSendData[1])); // и оправляем клиенту ответ
                }
                catch (Exception e)
                {
                    new LoggerClass().Logging(" Warning: некоторые параметры метода для отправки сообщений имеют ошибки : ", e); // опять логируем если поймали ошибку
                }
                finally
                {
                    client.Close(); // закрываем соединение
                }
            });
        }
    }
}

﻿using System.Threading;

namespace UdpServSys_4
{
    class Gatherer // класс - связка, по архитектуре что-то похожее на MVP :))) . Чистая "отсебятина"
    {
        UdpClass udp; 
        ImplementClass implement;
        Timer timer;

        public Gatherer(UdpClass _udp, ImplementClass _implement) // при создании объекта, передаем в параметр объекты класса udp и implement
        {
            udp = _udp; 
            implement = _implement;

            timer = new Timer(new TimerCallback(_implement.HandlerSendMessageAsync), null, 0, 3000);

            udp.ERxData += Udp_ERxData; // присваиваем событию приема данных метод обработчик
            implement.ETxData += Implement_ETxData; // присваиваем событию отправки данных
        }

        private void Implement_ETxData(string s) // сами методы обработчики события вызываю методы соответствующих классов с передачей параметров (в данном случае для отправки данных)
        {
            udp.SenderDataAsync(s);
        }

        private void Udp_ERxData(string s) // тоже самое только для приеема данных
        {
            implement.HandlerReceiverUdpAsync(s);
        }

        public Thread ThreadUdpReceiver() // метод запускает отдельный поток , в который передаем метод приема данных в виде аргумента, т.е. прием данных не идет в основном потоке
        {
            new LoggerClass().Logging(" Info: запуск модуля приема UDP-сообщений в отдельном потоке", null);
            return new Thread(udp.ReceiverData);
        }

    }
}

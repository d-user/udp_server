﻿using System;
using FirebirdSql.Data.FirebirdClient;
using Microsoft.Win32;

namespace UdpServSys_4
{
    interface IRequest // этими методами будем расширять класс работы с БД
    {
        string UpdateRequest(string ip, string port, string packet);
        string SelectMessageDev();
        void UpdateStatusDev();
    }


    class Request : IRequest // определим класс запроса данных к БД
    {
        public void UpdateStatusDev() // реализуем функцию обновления статуса клиента в БД
        {
            try
            {
                RegistryKey registry = Registry.CurrentUser; // обратимся к реестру, найдем там переменные пользователя по ключу "mfsVega" читаем там значения
                RegistryKey pathDb = registry.OpenSubKey("mfsVega");// значение будет содержать путь до БД
                string path = pathDb.GetValue("pathdb").ToString(); 
                pathDb.Close();

                using (FbConnection connection = new FbConnection(@"Server=localhost; User=SYSDBA; Password=masterkey; Database=" + path)) // создаем подключение к БД по пути и с данными 
                {
                    connection.Open(); // открываем соединения с ней

                    using (FbCommand command = new FbCommand("UPDATESTATUSDEV", connection)) // создаем команду для вызова хранимой процедуры
                    {
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        FbParameter paramDsc = new FbParameter("@desc_in", "Отсутствует связь с УК"); // в поле desc_in записываем значение
                        command.Parameters.Add(paramDsc); // добавляем параметр
                        command.ExecuteNonQuery(); // и вызываем процедуру
                    }

                }

            }
            catch(Exception e)
            {
                new LoggerClass().Logging(" ERROR: ошибка обращения к БД (вызов процедуры \'UPDATESTATUSDEV\' не осуществлен) - ", e); // при ошибке логируем
            }
        }

        public string SelectMessageDev() // метод, который осуществляет забор команды для отправки удаленному клиенту (В БД есть таблица куда попадают сообщения для отправки, проверяем ее, если у сообщения статус "не отправлен" выбираем их и отправляем)
        {
            string strResult = null; 

            try
            {
                RegistryKey registry = Registry.CurrentUser; 
                RegistryKey pathDb = registry.OpenSubKey("mfsVega");
                string path = pathDb.GetValue("pathdb").ToString();
                pathDb.Close();

                using (FbConnection connection = new FbConnection(@"Server=localhost; User=SYSDBA; Password=masterkey; Database=" + path)) // подключаемся
                {
                    connection.Open(); // открываем соединение

                    using (FbCommand command = new FbCommand("EXECUTECOMDEV", connection)) // создаем объект команды, открываем соединение с базой, выбираем хранимую процедуру и предаем ей строку-параметр
                    {
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        FbParameter paramDsc = new FbParameter("@desc_in", "Отправка команды УК");
                        command.Parameters.Add(paramDsc);
                        FbDataReader reader = command.ExecuteReader(); // читаем данные из таблицы БД

                        while (reader.Read())  // читаем пока есть в таблицы данные
                        {
                            if ((reader.GetString(0) != null) & (reader.GetString(1) != null))
                            {
                                strResult += reader.GetString(0) + "-" + reader.GetString(1) + "-" + reader.GetString(2) + "-" + reader.GetString(3); // объединяем сполученные результаты в единую строку с разделителем "-"
                                if (reader.FieldCount != 0) // если получили результат, то добавляем к строке ";"
                                {
                                    strResult += ";";
                                }
                            }
                        }
                        reader.Close(); закрываем соединения с базой 
                    }
                }

            }
            catch (Exception e)
            {
                new LoggerClass().Logging(" ERROR: ошибка обращения к БД (вызов процедуры \'EXECUTECOMDEV\' не осуществлен)- ", e);
            }
            return strResult; возвращаем результат
        }

        public string UpdateRequest(string ip, string port, string packet) // это метод обновления данных, пришедших отклиента
        {
            string[] strPackege = packet.Split(':');
            string strResult = null;

            try
            {
                RegistryKey registry = Registry.CurrentUser;
                RegistryKey pathDb = registry.OpenSubKey("mfsVega");
                string path = pathDb.GetValue("pathdb").ToString();
                pathDb.Close();

                using (FbConnection connection = new FbConnection(@"Server=localhost; User=SYSDBA; Password=masterkey; Database=" + path))
                {
                    connection.Open();

                    using (FbCommand command = new FbCommand("UPDATEDEV", connection)) // вызываем процедуру, передаем ей параметры, которые нужно обновить в таблице
                    {
                        command.CommandType = System.Data.CommandType.StoredProcedure; // формат пакета KEY:QOS:COUNTMSG:DATA
                        FbParameter paramIp = new FbParameter("@ip_in", ip);
                        command.Parameters.Add(paramIp);
                        FbParameter paramPort = new FbParameter("@port_in", port);
                        command.Parameters.Add(paramPort);
                        FbParameter paramMac = new FbParameter("@mac_in", strPackege[0]);
                        command.Parameters.Add(paramMac);
                        FbParameter paramCountM = new FbParameter("@countmsg_in", strPackege[2]);
                        command.Parameters.Add(paramCountM);
                        FbParameter paramDesc = new FbParameter("@descript_in", "Удаленный клиент");
                        command.Parameters.Add(paramDesc);
                        FbParameter paramDscOp = new FbParameter("@descript_in_aft", "Восстановление связи с УК");
                        command.Parameters.Add(paramDscOp);
                        FbParameter paramData = new FbParameter("@data_in", strPackege[3]);
                        command.Parameters.Add(paramData);
                        strResult = (command.ExecuteScalar()).ToString();
                        Console.WriteLine(strResult);
                    }
                }
            }
            catch (Exception e)
            {
                new LoggerClass().Logging(" ERROR: ошибка обращения к БД (вызов процедуры \'UPDATEDEV\' не осуществлен)- ", e);
            }

                if (strPackege[1] == "1") { return strPackege[0] + ":" + strPackege[1] + ":" + strPackege[2] + ":" + strResult; }
                else { return null; }
        }
    }
}

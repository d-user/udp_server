﻿using System;
using System.IO;
using System.Reflection;

namespace UdpServSys_4
{
    class LoggerClass // класс, методы которого реализуют логирование. Сначала проверяем есть ли файл-log, если нет- создаем (на каждый день свой лог), пишем туда. Хотя правильнее, наверное, этот класс делать  static
    {
        static object lockerError = new object();

        public void Logging(string s, Exception e)
        {
            lock(lockerError)
            {
                var pathLocation = Assembly.GetExecutingAssembly().Location;
                string[] strPathLoc = pathLocation.Split('\\');
                string strPathSet = null;
                string strPathLog = null;

                for (int i = 0; i < strPathLoc.Length; i++)
                {

                    if (!strPathLoc[i].Contains(".exe"))
                    {
                        strPathSet += strPathLoc[i] + @"\";
                    }
                    else if (strPathLoc[i].Contains(".exe"))
                    {
                        strPathLog = strPathSet + @"log";
                    }
                }

                DirectoryInfo directory = new DirectoryInfo(strPathLog);

                if (!directory.Exists)
                {
                    directory.Create();
                }

                string _date = DateTime.Now.ToShortDateString();

                if (!File.Exists(strPathLog + @"\" + _date + ".txt"))
                {
                    File.Create(strPathLog + @"\" + _date + ".txt").Dispose();
                }

                using (StreamWriter fileSetWrite = new StreamWriter(strPathLog + @"\" + DateTime.Now.ToShortDateString() + ".txt", true, System.Text.Encoding.Default))
                {
                    fileSetWrite.WriteLine(DateTime.Now + " -> " + s + e + " ;");
                    fileSetWrite.WriteLine("\n");
                }
            }
        }
    }
}
